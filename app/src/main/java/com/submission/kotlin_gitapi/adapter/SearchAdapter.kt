package com.submission.kotlin_gitapi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.submission.kotlin_gitapi.R
import com.submission.kotlin_gitapi.model.ModelData
import kotlinx.android.synthetic.main.item_view.view.*

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    inner class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(md: ModelData) {
            with(itemView) {
                Glide.with(itemView.context)
                    .load(md.avatar)
                    .apply(RequestOptions()
                        .override(100, 100)
                        .placeholder(R.drawable.ic_sync_50)
                        .error(R.drawable.ic_error_50))
                    .into(civ)
                iv_tv_username.text = md.login
                iv_tv_url.text = md.url

                itemView.setOnClickListener { onItemClickCallback?.onItemClick(md) }
            }
        }
    }

    private val modelData = ArrayList<ModelData>()
    fun setData(data: ArrayList<ModelData>) {
        modelData.clear()
        modelData.addAll(data)
        notifyDataSetChanged()
    }

    private var onItemClickCallback: OnItemClickCallback? = null

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClick(selectedData: ModelData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return SearchViewHolder(view)
    }

    override fun getItemCount(): Int = modelData.size

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(modelData[position])
    }


}