package com.submission.kotlin_gitapi.adapter

import android.content.Context
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.submission.kotlin_gitapi.R
import com.submission.kotlin_gitapi.fragment.FollowersFragment
import com.submission.kotlin_gitapi.fragment.FollowingFragment

class SectionPagerAdapter(private val mContext: Context, fm: FragmentManager): FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var username: String?= "username"

    @StringRes
    private  val TAB_TITLES = intArrayOf(R.string.tab_followers, R.string.tab_following)

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when(position) {
            0 -> {
                fragment = FollowersFragment()
                val collection = Bundle()
                collection.putString(FollowersFragment.SELECT_USER, username)
                fragment.arguments= collection
            }
                1 -> {
                    fragment = FollowingFragment()
                    val collection= Bundle()
                    collection.putString(FollowingFragment.SELECT_USER, username)
                    fragment.arguments= collection
                }


        }
        return  fragment as Fragment
    }

    fun setName(anonym: String) {
        username= anonym
    }
    

    @Nullable
    override fun getPageTitle(position: Int): CharSequence? {
        return mContext.resources.getString(TAB_TITLES[position])    }

    override fun getCount(): Int {
       return 2
    }
}