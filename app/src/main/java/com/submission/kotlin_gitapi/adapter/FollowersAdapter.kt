package com.submission.kotlin_gitapi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.submission.kotlin_gitapi.R
import com.submission.kotlin_gitapi.model.ModelData
import kotlinx.android.synthetic.main.item_fragment.view.*

class FollowersAdapter: RecyclerView.Adapter<FollowersAdapter.FollowersViewHolder>(){

    class FollowersViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(md: ModelData) {
            with(itemView) {
                Glide.with(itemView.context)
                    .load(md.avatar)
                    .apply(RequestOptions()
                        .placeholder(R.drawable.ic_sync_50)
                        .error(R.drawable.ic_error_50))
                    .into(civ_fragment)
                iv_tv_username_fragment.text= md.login
                iv_tv_organizations_fragment.text= md.company
            }
        }
    }

    private val modelData= ArrayList<ModelData>()
    fun setData(data: ArrayList<ModelData>) {
        modelData.clear()
        modelData.addAll(data)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FollowersViewHolder {
        val view= LayoutInflater.from(parent.context).inflate(R.layout.item_fragment, parent, false)
        return FollowersViewHolder(view)
    }

    override fun getItemCount(): Int= modelData.size

    override fun onBindViewHolder(holder: FollowersViewHolder, position: Int) {
        holder.bind(modelData[position])
    }



}