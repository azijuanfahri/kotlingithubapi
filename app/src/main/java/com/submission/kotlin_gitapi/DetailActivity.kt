package com.submission.kotlin_gitapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.submission.kotlin_gitapi.adapter.SectionPagerAdapter
import com.submission.kotlin_gitapi.fragment.FollowersFragment
import com.submission.kotlin_gitapi.fragment.FollowingFragment
import com.submission.kotlin_gitapi.model.ModelData
import com.submission.kotlin_gitapi.network.DetailViewModel
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    companion object{
        internal val TAG = DetailActivity::class.java.simpleName
        const val EXTRA_DATA = "extra_data"
        const val BACK_TO_MAIN = "back_to_main"
        const val BACK_TO_FAV = "back_to_fav"
    }

    private var modelData: ModelData? = null
    private var optionMenu: Menu? = null
    private lateinit var detailViewModel: DetailViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        supportActionBar?.title = "Detail User"

        modelData= intent.getParcelableExtra(EXTRA_DATA) as ModelData
        Glide.with(this)
            .load(modelData?.avatar)
            .apply(RequestOptions
                .placeholderOf(R.drawable.ic_sync_50))
                .error(R.drawable.ic_error_50)
            .into(civ_profile_detail)

        detailViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(DetailViewModel::class.java)
        detailViewModel.setDetailPerson(modelData?.login.toString())
        detailViewModel.getPersonDetail().observe(this, Observer { modelData ->
            if (modelData != null) {
                tv_name_detail.text= modelData.name
                tv_company_detail.text=  modelData.company
                tv_publicrepos_detail.text= modelData.public_repos.toString()
                tv_followers_detail.text= modelData.followers_total.toString()
                tv_following_detail.text= modelData.following_total.toString()
            }
        })

        setFragment(modelData!!)

    }

    private fun setFragment(modelData: ModelData) {
        val collection= Bundle()
        val followers= FollowersFragment()
        collection.putString(FollowersFragment.SELECT_USER, modelData.login)
        followers.arguments= collection

        val following= FollowingFragment()
        collection.putString(FollowingFragment.SELECT_USER, modelData.login)
        following.arguments= collection

        val spa= SectionPagerAdapter(this, supportFragmentManager)
        spa.setName(modelData.login.toString())
        viewPager.adapter= spa
        tabLayout.setupWithViewPager(viewPager)
    }
}

