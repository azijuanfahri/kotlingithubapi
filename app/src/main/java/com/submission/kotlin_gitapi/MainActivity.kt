package com.submission.kotlin_gitapi

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.submission.kotlin_gitapi.adapter.SearchAdapter
import com.submission.kotlin_gitapi.model.ModelData
import com.submission.kotlin_gitapi.network.BindingItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private  lateinit var adapter: SearchAdapter
    private lateinit var bindingItem: BindingItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = SearchAdapter()
        adapter.notifyDataSetChanged()
        
        bindingItem = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(BindingItem::class.java)
        bindingItem.getPersonItem().observe(this, Observer { modelData ->
            if (modelData != null) {
                adapter.setData(modelData)
                showLoading(false)
                rv_main.visibility = View.VISIBLE

                rv_main.layoutManager = LinearLayoutManager(this)
                rv_main.adapter = adapter

                adapter.setOnItemClickCallback(object: SearchAdapter.OnItemClickCallback{
                    override fun onItemClick( modelData: ModelData) {
                        val detail = Intent(this@MainActivity, DetailActivity::class.java)
                        detail.putExtra(DetailActivity.EXTRA_DATA, modelData)
                        detail.putExtra(DetailActivity.BACK_TO_MAIN, "main_activity")
                        startActivity(detail)

                    }

                })
            }
        })
    }


    private fun showLoading(state: Boolean) {
        if (state) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.option_menu, menu)

        val sManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val sView = menu.findItem(R.id.search_bar_id).actionView as SearchView

        sView.setSearchableInfo(sManager.getSearchableInfo(componentName))
        sView.queryHint = resources.getString(R.string.type_here)
        sView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(keyword: String): Boolean {
                if (keyword.isEmpty()) {
                    return true
                } else {
                    showLoading(true)
                    bindingItem.setSearchItem(keyword)
                }
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
               return false
            }

        })

        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.favorite_bar -> {
                val move = Intent(this, FavoriteActivity::class.java)
                startActivity(move)
                return true
            }

            R.id.setting_bar -> {
                val move = Intent(this, SettingActivity::class.java)
                startActivity(move)
                return true
            } else -> return false
        }
    }

}