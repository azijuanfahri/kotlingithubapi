package com.submission.kotlin_gitapi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.submission.kotlin_gitapi.R
import com.submission.kotlin_gitapi.adapter.FollowingAdapter
import com.submission.kotlin_gitapi.network.FollowingViewModel
import kotlinx.android.synthetic.main.following_fragment.*

class FollowingFragment : Fragment() {

    companion object {
        const val SELECT_USER= "select_user"
    }

    private lateinit var viewModel: FollowingViewModel
    private lateinit var adapter: FollowingAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.following_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel= ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            FollowingViewModel::class.java)

        if (arguments != null) {
            val name= arguments?.getString(SELECT_USER)
            viewModel.setSelectedPerson(name.toString())
        }

        viewModel.getFollowingItem().observe(viewLifecycleOwner, Observer { modelData ->
            if (modelData != null) {
                adapter.setData(modelData)
            }
        })

        adapter= FollowingAdapter()
        rv_fragment_following.layoutManager= LinearLayoutManager(context)
        rv_fragment_following.adapter= adapter
        adapter.notifyDataSetChanged()
    }

}