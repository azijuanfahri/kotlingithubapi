package com.submission.kotlin_gitapi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.submission.kotlin_gitapi.R
import com.submission.kotlin_gitapi.adapter.FollowersAdapter
import com.submission.kotlin_gitapi.network.FollowersViewModel
import kotlinx.android.synthetic.main.followers_fragment.*

class FollowersFragment : Fragment() {

    companion object{
        const val SELECT_USER= "select_user"
    }

    private lateinit var viewModel: FollowersViewModel
    private lateinit var adapter: FollowersAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.followers_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel= ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            FollowersViewModel::class.java)

        if (arguments != null) {
            val name= arguments?.getString(SELECT_USER)
            viewModel.setSelectedPerson(name.toString())
        }

        viewModel.getFollowersItem().observe(viewLifecycleOwner, Observer {  modelData ->
            if (modelData != null) {
                adapter.setData(modelData)
            }
        })
        adapter= FollowersAdapter()
        rv_fragment_followers.layoutManager= LinearLayoutManager(context)
        rv_fragment_followers.adapter= adapter
        adapter.notifyDataSetChanged()
    }

}