package com.submission.kotlin_gitapi.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.TextHttpResponseHandler
import com.submission.kotlin_gitapi.model.ModelData
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class BindingItem: ViewModel() {

    val listSearch = MutableLiveData<ArrayList<ModelData>>()

    fun setSearchItem(keyword: String) {
        val listPerson = ArrayList<ModelData>()

        val asyncClient = AsyncHttpClient()
        asyncClient.addHeader("Authorization", "token 2ad83c54eb722edeeae4c2ee562024b4459852d7")
        asyncClient.addHeader("User-Agent", "request")
        asyncClient.get("https://api.github.com/search/users?q=$keyword", object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray
            ) {
                try {
                    val result= String(responseBody)
                    val responseObject= JSONObject(result)
                    val list = responseObject.getJSONArray("items")

                     for (i in 0 until list.length()) {
                        val person= list.getJSONObject(i)
                        val modelData= ModelData()
                        modelData.avatar = person.getString("avatar_url")
                        modelData.login = person.getString("login")
                        modelData.url = person.getString("url")
                        listPerson.add(modelData)
                    }

                    listSearch.postValue(listPerson)
                } catch (e: Exception) {
                    Log.d("Exception", e.message.toString())
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray,
                error: Throwable
            ) {
                Log.d("Failure", error.message.toString())
            }

        })
}

    fun getPersonItem(): LiveData<ArrayList<ModelData>>{
        return listSearch
    }
}

