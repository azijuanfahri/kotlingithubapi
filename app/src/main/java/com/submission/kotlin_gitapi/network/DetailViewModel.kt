package com.submission.kotlin_gitapi.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.submission.kotlin_gitapi.model.ModelData
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class DetailViewModel: ViewModel(){

    private val detailData = MutableLiveData<ModelData>()

    fun setDetailPerson(selected: String) {
        val asyncClient = AsyncHttpClient()
        asyncClient.addHeader("Authorization", "token 2ad83c54eb722edeeae4c2ee562024b4459852d7")
        asyncClient.addHeader("User-Agent", "request")
        asyncClient.get("https://api.github.com/users/$selected", object : AsyncHttpResponseHandler(){
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray
            ) {
                try {
                    var result= String(responseBody)
                    val responseObject= JSONObject(result)

                    val modelData= ModelData()
                    modelData.login= responseObject.getString("login")
                    modelData.name= responseObject.getString("name")
                    modelData.avatar= responseObject.getString("avatar_url")
                    modelData.company= responseObject.getString("company")
                    modelData.public_repos= responseObject.getInt("public_repos")
                    modelData.followers_total= responseObject.getInt("followers")
                    modelData.following_total= responseObject.getInt("following")
                    modelData.location= responseObject.getString("location")
                    detailData.postValue(modelData)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable
            ) {
                Log.d("Failure", error.message.toString())
            }

        })
    }

    fun getPersonDetail(): LiveData<ModelData>{
        return detailData
    }

}