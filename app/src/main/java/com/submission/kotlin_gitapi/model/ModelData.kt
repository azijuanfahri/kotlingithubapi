package com.submission.kotlin_gitapi.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelData(
    var id: Int = 0,
    var login: String?= null,
    var name: String?= null,
    var avatar: String?= null,
    var url: String?= null,
    var followers: String?= null,
    var following: String?= null,
    var followers_total: Int?= null,
    var following_total: Int?= null,
    var subs: String?= null,
    var company: String?= null,
    var location: String?= null,
    var public_repos: Int?= null

): Parcelable